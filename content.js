'use strict';

InboxSDK.load(2, 'sdk_VentClick_b9ddc795b9').then(sdk => {
	sdk.Toolbars.addToolbarButtonForApp({
		title: "",
		iconUrl: 'https://gitlab.com/moflesillo/extension_bxm/raw/master/icon64.png',
		iconClass: 'icon',
		onClick: function(DropdownView){
			popup(DropdownView);
		}
	});
	sdk.Conversations.registerThreadViewHandler(threadView => {
		getSideBar(threadView);
	});
	// sdk.Conversations.registerThreadViewHandler(threadView => {
	// 	var bar = threadView.addNoticeBar();
	// 	$(bar.el).css("background-color","green");
	// });

	// sdk.Toolbars.registerThreadButton({
	// 	title : "Titulo",
	// 	iconUrl: 'https://gitlab.com/moflesillo/extension_bxm/raw/master/icon64.png',
	// 	positions: '["LIST"]',
	// 	onClick: function(DropdownView){
	// 		console.log(DropdownView);
	// 	}
	// });

	// sdk.Compose.registerComposeViewHandler(function(composeView){
 //    	composeView.addButton({
 //    		title : "Titulo",
	// 		iconUrl: 'https://gitlab.com/moflesillo/extension_bxm/raw/master/icon64.png',
	// 		onClick: function(){
	// 			console.log("clickaddbuton");
	// 		}
 //    	});

 //  	});

  	sdk.Search.registerSearchSuggestionsProvider(function(query){
  		return [
  			{
  				nameHTML: '<div><p>Jorge Carpizo</p></div>',
  				descriptionHTML: '<span>Contacto: jorgecarpizo@pruebas.com</span>',
  				iconUrl: 'https://gitlab.com/moflesillo/extension_bxm/raw/master/icon32.png',
  				externalURL: 'https://marketing.clickbalance.com'
  			},
  			{
  				nameHTML: '<div><p>Oportunidad: Servicio de consultoría Contacto: Jorge Carpizo (jorgecarpizo@pruebas.com</p></div>',
  				descriptionHTML: '<span>Oportunidad</span>',
  				iconUrl: 'https://gitlab.com/moflesillo/extension_bxm/raw/master/icon32.png',
  				externalURL: 'https://marketing.clickbalance.com'
  			},
  			{
  				nameHTML: '<div><p>Empresa: Consultoria S.C. Contacto: Jorge Carpizo (jorgecarpizo@pruebas.com</p></div>',
  				descriptionHTML: '<span>Empresa</span>',
  				iconUrl: 'https://gitlab.com/moflesillo/extension_bxm/raw/master/icon32.png',
  				externalURL: 'https://marketing.clickbalance.com'
  			}
		];

  	});
	
});

function popup(DropdownView){
	var template = Handlebars.templates['user'];
	var data = {
		name : 'José Luis',
		surname: 'Arellano'
	};
	var html= template(data);
	$(DropdownView.dropdown.el).html(html);
	$("#changeaccount").click(function(event) {
		getChangeAccount(DropdownView);
	});
}

function getChangeAccount(DropdownView){
	var template = Handlebars.templates['changeAccount'];
	var html =  template();
	$(DropdownView.dropdown.el).html(html);
	$("#main").click(function(event) {
		popup(DropdownView);
	});
	return html;
}

function getSideBar(threadView){
	var template = Handlebars.templates['sidebar'];
	var html= template();
	const el = document.createElement("div");
	el.classList.add("content");
	el.innerHTML = html;


	 const panel = threadView.addSidebarContentPanel({
			title: 'Sidebar Example',
			iconUrl: 'https://gitlab.com/moflesillo/extension_bxm/raw/master/icon64.png',
			el,
			hideTitleBar: true
		});
	 console.log(panel);
	panel.once('activate', () => {
	  buildHTMLShellListeners();
	});
}


// const el = document.createElement("div");
// 		el.innerHTML = 'Hello world!';

// 		threadView.addToolbarButtonForApp({
// 			title: 'Sidebar Example',
// 			iconUrl: chrome.runtime.getURL('icon.png'),
// 			onClick: function(){
// 				alert("hola");
// 			}
// 		});